package bluebear.ex7_databasesave;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void showListAct(View v)
    {
        Intent i = new Intent(this,ListAct.class);
        startActivity(i);
    }
    public void showInsAct(View v){
        Intent i = new Intent(this,INS.class);
        startActivity(i);
    }
}
